import requests
from bs4 import BeautifulSoup
from random import choice, uniform
from time import sleep
from time import time
import datetime

def get_html(url, useragent=None, proxy=None):
    r = requests.get(url, headers=useragent, proxies=proxy)
    return r.text

def get_ip(html):
    soup = BeautifulSoup(html, 'lxml')
    music_div = soup.find_all('div', class_="d-track__name")

    music_arr = []

    music_gl = []

    for i in music_div:
        music = i.get('title')

        music_arr.append(music)

        music_gl = music_arr[0:3] 

        #print(music_gl)

    return music_gl


def main():
    #url = 'http://sitespy.ru/my-ip'
    url = 'https://music.yandex.ru/users/marktamarov/playlists/3'
    
    useragents = open('useragents.txt').read().split('\n')
    proxies = open('proxies.txt').read().split('\n')

    for i in range(50):
        # sleep(uniform(3, 6))
        proxy = {'http': 'http://' + choice(proxies)}
        useragent = {'User-Agent': choice(useragents)}
        try:
            html = get_html(url, useragent, proxy)
        except:
            continue
        music_new = get_ip(html)

        if (len(music_new) > 0):
            print(music_new)
            break
    #return music_new


    
        

if __name__ == '__main__':
    main()